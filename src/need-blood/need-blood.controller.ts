import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { NeedBloodService } from './need-blood.service';
import { CreateNeedBloodDto } from './dto/create-need-blood.dto';
import { UpdateNeedBloodDto } from './dto/update-need-blood.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('need-blood')
export class NeedBloodController {
  constructor(private readonly needBloodService: NeedBloodService) {}

  @Post()
  @UseGuards(AuthGuard('jwt'))
  create(@Body() createNeedBloodDto: CreateNeedBloodDto) {
    return this.needBloodService.create(createNeedBloodDto);
  }

  @Get()
  @UseGuards(AuthGuard('jwt'))
  findAll() {
    return this.needBloodService.findAll();
  }

  @Get(':id')
  @UseGuards(AuthGuard('jwt'))
  findOne(@Param('id') id: string) {
    return this.needBloodService.findOne(+id);
  }

  @Patch(':id')
  @UseGuards(AuthGuard('jwt'))
  update(
    @Param('id') id: string,
    @Body() updateNeedBloodDto: UpdateNeedBloodDto,
  ) {
    return this.needBloodService.update(+id, updateNeedBloodDto);
  }

  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  remove(@Param('id') id: string) {
    return this.needBloodService.remove(+id);
  }
}
