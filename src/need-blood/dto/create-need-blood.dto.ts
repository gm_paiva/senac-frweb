export class CreateNeedBloodDto {
  name: string;
  email: string;
  phone: number;
  bloodType: string;
  placeDonate: string;
}
