import { PartialType } from '@nestjs/mapped-types';
import { CreateNeedBloodDto } from './create-need-blood.dto';

export class UpdateNeedBloodDto extends PartialType(CreateNeedBloodDto) {}
