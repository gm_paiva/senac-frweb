import { DataSource } from 'typeorm';
import { NeedBlood } from './entities/need-blood.entity';

export const needBloodProviders = [
  {
    provide: 'NEED_BLOOD_REPOSITORY',
    useFactory: (dataSource: DataSource) => dataSource.getRepository(NeedBlood),
    inject: ['DATA_SOURCE'],
  },
];
