import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class NeedBlood {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column()
  name: string;

  @Column()
  email: string;

  @Column()
  phone: number;

  @Column()
  bloodType: string;

  @Column()
  placeDonate: string;
}
