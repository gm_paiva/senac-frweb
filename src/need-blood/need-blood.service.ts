import { Inject, Injectable } from '@nestjs/common';
import { CreateNeedBloodDto } from './dto/create-need-blood.dto';
import { UpdateNeedBloodDto } from './dto/update-need-blood.dto';
import { Repository } from 'typeorm';
import { NeedBlood } from './entities/need-blood.entity';

@Injectable()
export class NeedBloodService {
  constructor(
    @Inject('NEED_BLOOD_REPOSITORY')
    private needBloodRepository: Repository<NeedBlood>,
  ) {}
  create(createNeedBloodDto: CreateNeedBloodDto) {
    return this.needBloodRepository.save(createNeedBloodDto);
  }

  findAll() {
    return this.needBloodRepository.find();
  }

  findOne(id: number) {
    return this.needBloodRepository.findOneBy({ id: id });
  }

  update(id: number, updateNeedBloodDto: UpdateNeedBloodDto) {
    return this.needBloodRepository.update(id, updateNeedBloodDto);
  }

  remove(id: number) {
    return this.needBloodRepository.delete(id);
  }
}
