import { Module, forwardRef } from '@nestjs/common';
import { NeedBloodService } from './need-blood.service';
import { NeedBloodController } from './need-blood.controller';
import { AppModule } from 'src/app.module';
import { needBloodProviders } from './need-blood.provider';

@Module({
  imports: [forwardRef(() => AppModule)],
  providers: [...needBloodProviders, NeedBloodService],
  controllers: [NeedBloodController],
})
export class NeedBloodModule {}
