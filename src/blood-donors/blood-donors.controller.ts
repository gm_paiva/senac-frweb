import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { BloodDonorsService } from './blood-donors.service';
import { CreateBloodDonorDto } from './dto/create-blood-donor.dto';
import { UpdateBloodDonorDto } from './dto/update-blood-donor.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('blood-donors')
export class BloodDonorsController {
  constructor(private readonly bloodDonorsService: BloodDonorsService) {}

  @Post()
  @UseGuards(AuthGuard('jwt'))
  create(@Body() createBloodDonorDto: CreateBloodDonorDto) {
    return this.bloodDonorsService.create(createBloodDonorDto);
  }

  @Get()
  @UseGuards(AuthGuard('jwt'))
  findAll() {
    return this.bloodDonorsService.findAll();
  }

  @Get(':id')
  @UseGuards(AuthGuard('jwt'))
  findOne(@Param('id') id: string) {
    return this.bloodDonorsService.findOne(+id);
  }

  @Patch(':id')
  @UseGuards(AuthGuard('jwt'))
  update(
    @Param('id') id: string,
    @Body() updateBloodDonorDto: UpdateBloodDonorDto,
  ) {
    return this.bloodDonorsService.update(+id, updateBloodDonorDto);
  }

  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  remove(@Param('id') id: string) {
    return this.bloodDonorsService.remove(+id);
  }
}
