import { DataSource } from 'typeorm';
import { BloodDonor } from './entities/blood-donor.entity';

export const bloodDonorsProviders = [
  {
    provide: 'BLOOD_DONORS_REPOSITORY',
    useFactory: (dataSource: DataSource) =>
      dataSource.getRepository(BloodDonor),
    inject: ['DATA_SOURCE'],
  },
];
