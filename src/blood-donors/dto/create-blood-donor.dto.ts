export class CreateBloodDonorDto {
  name: string;
  email: string;
  phone: number;
  state: string;
  city: string;
  sex: string;
  bloodType: string;
}
