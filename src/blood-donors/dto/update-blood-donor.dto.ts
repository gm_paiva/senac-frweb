import { PartialType } from '@nestjs/mapped-types';
import { CreateBloodDonorDto } from './create-blood-donor.dto';

export class UpdateBloodDonorDto extends PartialType(CreateBloodDonorDto) {}
