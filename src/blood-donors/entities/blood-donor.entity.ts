import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class BloodDonor {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column()
  name: string;

  @Column()
  email: string;

  @Column()
  phone: number;

  @Column()
  state: string;

  @Column()
  city: string;

  @Column()
  sex: string;

  @Column()
  bloodType: string;
}
