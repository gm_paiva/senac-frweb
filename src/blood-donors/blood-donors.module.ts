import { Module, forwardRef } from '@nestjs/common';
import { BloodDonorsService } from './blood-donors.service';
import { BloodDonorsController } from './blood-donors.controller';
import { bloodDonorsProviders } from './blood-donors.provider';
import { AppModule } from 'src/app.module';
import { JwtModule } from '@nestjs/jwt';

@Module({
  imports: [forwardRef(() => AppModule), JwtModule.register({})],
  providers: [...bloodDonorsProviders, BloodDonorsService],
  controllers: [BloodDonorsController],
})
export class BloodDonorsModule {}
