import { Inject, Injectable } from '@nestjs/common';
import { CreateBloodDonorDto } from './dto/create-blood-donor.dto';
import { UpdateBloodDonorDto } from './dto/update-blood-donor.dto';
import { Repository } from 'typeorm';
import { BloodDonor } from './entities/blood-donor.entity';

@Injectable()
export class BloodDonorsService {
  constructor(
    @Inject('BLOOD_DONORS_REPOSITORY')
    private bloodDonorsRepository: Repository<BloodDonor>,
  ) {}
  create(createBloodDonorDto: CreateBloodDonorDto) {
    return this.bloodDonorsRepository.save(createBloodDonorDto);
  }
  findAll(): Promise<BloodDonor[]> {
    return this.bloodDonorsRepository.find();
  }

  findOne(id: number) {
    return this.bloodDonorsRepository.findOneBy({ id: id });
  }

  update(id: number, updateBloodDonorDto: UpdateBloodDonorDto) {
    return this.bloodDonorsRepository.update(id, updateBloodDonorDto);
  }

  remove(id: number) {
    return this.bloodDonorsRepository.delete(id);
  }
}
