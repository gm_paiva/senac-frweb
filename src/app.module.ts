import { Module, forwardRef } from '@nestjs/common';
import { databaseProviders } from '../ormconfig.provider';
import { BloodDonorsModule } from './blood-donors/blood-donors.module';
import { NeedBloodModule } from './need-blood/need-blood.module';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';

@Module({
  providers: [...databaseProviders],
  exports: [...databaseProviders],
  imports: [
    forwardRef(() => BloodDonorsModule),
    forwardRef(() => NeedBloodModule),
    forwardRef(() => UsersModule),
    forwardRef(() => AuthModule),
  ],
  controllers: [],
})
export class AppModule {}
