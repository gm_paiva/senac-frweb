//auth.module.ts
import { Module, forwardRef } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { UsersService } from 'src/users/users.service';
import { AuthService } from './auth.service';
import { AppModule } from 'src/app.module';
import { usersProviders } from 'src/users/users.provider';
import { AuthController } from './auth.controller';
import { JwtStrategy } from './strategies/jwt-strategy';

@Module({
  imports: [
    forwardRef(() => AppModule),
    // TypeOrmModule.forFeature([User]),
    forwardRef(() => JwtModule),
    JwtModule.register({}),
  ],
  controllers: [AuthController],
  providers: [...usersProviders, AuthService, UsersService, JwtStrategy],
})
export class AuthModule {}
