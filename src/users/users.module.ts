import { Module, forwardRef } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { usersProviders } from './users.provider';
import { AppModule } from 'src/app.module';
import { JwtModule } from '@nestjs/jwt';
import { AuthService } from 'src/auth/auth.service';
import { JwtStrategy } from 'src/auth/strategies/jwt-strategy';

@Module({
  imports: [forwardRef(() => AppModule), JwtModule.register({})],
  controllers: [UsersController],
  providers: [...usersProviders, UsersService, AuthService, JwtStrategy],
})
export class UsersModule {}
