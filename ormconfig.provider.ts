import { DataSource } from 'typeorm';

export const databaseProviders = [
  {
    provide: 'DATA_SOURCE',
    useFactory: async () => {
      const dataSource = new DataSource({
        type: 'sqlite',
        database: '.db/sql',
        synchronize: true,
        entities: [__dirname + '/**/*.entity{.ts,.js}'],
      });

      return dataSource.initialize();
    },
  },
];
